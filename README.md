# Todeslang docs

[言語仕様](https://gitlab.com/todeslang/todeslang-spec)

[*todes* コマンド](todes-command.md)

[ウェブアプリケーションプラットフォームTodesweb](todesweb.md)

[外部プログラムの実行](exec.md)

[ファイル名の規則](file-name-standard.md)

[ファイル入出力](file.md)

[フォーマッターとスキャナー](formatter.md)

[その他](misc.md)

