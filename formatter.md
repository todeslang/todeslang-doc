# フォーマッターとスキャナー

## フォーマッター

_format_ プリミティブ演算は、以下の引数を取り、文字列を返す。

_main_ 引数は必須で、自然数または浮動小数点数である必要がある。

_precision_ 引数と _width_ 引数は省略可で、省略しないならば自然数である必要がある。

以下の引数は省略可で、省略しないならばブーリアンである必要がある。

* boolalpha
* dec
* defaultfloat
* fixed
* hex
* hexfloat
* internal
* left
* noboolalpha
* noshowbase
* noshowpoint
* noshowpos
* noskipws
* nounitbuf
* nouppercase
* oct
* right
* scientific
* showbase
* showpoint
* showpos
* skipws
* unitbuf
* uppercase

## スキャナー

_scan_ プリミティブ演算は、以下の引数を取り、自然数または浮動小数点数を返す。

_main_ 引数は必須で、文字列である必要がある。

_type_ 引数は必須で、_number_ または _float_ という文字列である必要がある。

_precision_ 引数と _width_ 引数は省略可で、省略しないならば自然数である必要がある。

以下の引数は省略可で、省略しないならばブーリアンである必要がある。

* boolalpha
* dec
* defaultfloat
* fixed
* hex
* hexfloat
* internal
* left
* noboolalpha
* noshowbase
* noshowpoint
* noshowpos
* noskipws
* nounitbuf
* nouppercase
* oct
* right
* scientific
* showbase
* showpoint
* showpos
* skipws
* unitbuf
* uppercase
