# ファイル入出力

ファイルの入出力は、ファイルカプセルを通して行う。ファイルカプセルはカプセルである。

_File_ プリミティブ演算は、ファイルカプセルを返す。

ファイルカプセルの _File_ メソッドは真を返す。

ファイルカプセルの _readopen_ メソッドは、ファイルを読み取りモードで開く。文字列型の _main_ 引数を取り、ファイル名として解釈する。

ファイルカプセルの _writeopen_ メソッドは、ファイルを書き込みモードで開く。文字列型の _main_ 引数を取り、ファイル名として解釈する。

ファイルカプセルの _appendopen_ メソッドは、ファイルを追記モードで開く。文字列型の _main_ 引数を取り、ファイル名として解釈する。

ファイルカプセルの _close_ メソッドは、開いているファイルを閉じる。引数を取らない。

ファイルカプセルの _stdin_ メソッドは、標準入力を操作対象とする。引数を取らない。

ファイルカプセルの _stdout_ メソッドは、標準出力を操作対象とする。引数を取らない。

ファイルカプセルの _stderr_ メソッドは、標準エラー出力を操作対象とする。引数を取らない。

ファイルカプセルの _isopen_ メソッドは、ファイルを開いているかどうかを、真または偽で返す。引数を取らない。

ファイルカプセルの _readstring_ メソッドは、ファイルを終端まで読み取って文字列を返す。引数を取らない。

ファイルカプセルの _readbinary_ メソッドは、ファイルを終端まで読み取って配列を返す。配列の各要素は、0以上100 (16進法) 未満の自然数である。

ファイルカプセルの _writestring_ メソッドは、ファイルに文字列を書き込む。文字列型の _main_ 引数を取る。

ファイルカプセルの _writebinary_ メソッドは、ファイルにバイナリを書き込む。配列の _main_ 引数を取る。配列の各要素は、0以上100 (16進法) 未満の自然数である必要がある。

